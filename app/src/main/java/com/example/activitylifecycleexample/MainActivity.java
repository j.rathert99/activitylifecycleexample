package com.example.activitylifecycleexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView text;
    private Button clear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text =  (TextView)findViewById(R.id.LogText);

        text.append("onCreate called\n");
        System.out.println("onCreate called");

        clear = (Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText("");
            }
        });

        final Button button = (Button) findViewById(R.id.goNext);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent switchActivityIntent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(switchActivityIntent);
            }
        });
    }

    public String getText() {
        return text.getText().toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(text != null)
            text.append("onStart called\n");
        System.out.println("onStart called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(text != null)
            text.append("onStop called\n");
        System.out.println("onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(text != null)
            text.append("onDestroy called\n");
        System.out.println("onDestroy called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(text != null)
            text.append("onResume called\n");
        System.out.println("onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(text != null)
            text.append("onPause called\n");
        System.out.println("onPause called");
    }
}