package com.example.activitylifecycleexample;

import android.widget.TextView;

import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(AndroidJUnit4.class)
public class EditTextTest {

    @Test
    public void editTextTest() {
        try(ActivityScenario<MainActivity> scenario = ActivityScenario.launch(MainActivity.class)) {
            System.out.println("Start Test");
            scenario.onActivity(activity -> {
                String text = activity.getText();


                System.out.println("Text in app:");
                System.out.println("text");
            });
            onView(withId(R.id.goNext)).perform(click());
            System.out.println("End Test");
        }

        //onView(withId(R.id.editText_main)).check(matches(withText(containsString("SOME_TEXT"))));
    }


}
